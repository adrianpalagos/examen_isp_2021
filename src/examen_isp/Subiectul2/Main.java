package examen_isp.Subiectul2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    public static void main(String[] args) {
        new Window();
    }
}

class Window extends JFrame {

    private JButton button;
    private TextField textField1;
    private TextField textField2;
    private TextField textField3;

    public Window() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Subiectul2");
        init();
        setSize(500, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        button = new JButton("Press");
        button.setBounds(150, 350, 200, 100);
        button.addActionListener(new Action());

        textField1 = new TextField();
        textField1.setBounds(100, 80, 100, 50);

        textField2 = new TextField();
        textField2.setBounds(100, 180, 100, 50);

        textField3 = new TextField();
        textField3.setBounds(100, 280, 100, 50);
        textField3.setEditable(false);

        add(button);
        add(textField1);
        add(textField2);
        add(textField3);
    }

    class Action implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            int a = Integer.parseInt(textField1.getText());
            int b = Integer.parseInt(textField2.getText());

            int c = a * b;
            textField3.setText(Integer.toString(c));
        }
    }
}